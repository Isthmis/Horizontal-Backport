# Horizontal Backport

![](resources/logo.png)

![](resources/all_blocks.png)

## Minecraft Forge 1.7.10 mod

[EN]  
The mod backports the following functions by scratching and rewriting "[Horizontal Glass Panes](https://www.curseforge.com/minecraft/mc-mods/horizontal-glass-panes)".  
(If you are using the latest version Minecraft, their mod is all you need.)

- Block transparency
- Horizontal iron bars
- Same blast resistance and hardness as vanilla

[JP]  
「[Horizontal Glass Panes](https://www.curseforge.com/minecraft/mc-mods/horizontal-glass-panes)」を新しく書き直したことにより、下記の機能をバックポートしたmodです。  
(最新版のマインクラフトを使っている場合、前述のMODがあれば十分です。)

- 透過ブロック
- 鉄格子
- バニラと同じ爆発耐性・硬度

![](resources/diff.png)

## Compatibility

Block ID **COMPATIBLE**!

## Requires

- Minecraft 1.7.10
- Forge 10.13.4.1614

## License

MIT License

## Credits

This mod is inspired by [Horizontal Glass Panes](https://www.curseforge.com/minecraft/mc-mods/horizontal-glass-panes) by witchica.
