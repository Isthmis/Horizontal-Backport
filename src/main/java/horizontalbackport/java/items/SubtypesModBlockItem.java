package horizontalbackport.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public class SubtypesModBlockItem extends ItemBlock {
    public SubtypesModBlockItem(Block block) {
        super(block);
        this.setHasSubtypes(true);
    }

    @Override
    public int getMetadata(int damage) {
        return damage;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIconFromDamage(int damage) {
        return (this.field_150939_a).getIcon(0, damage);
    }

    @Override
    public String getItemStackDisplayName(ItemStack itemStack) {
        return new ItemStack(Blocks.stained_glass_pane, 1, itemStack.getItemDamage()).getDisplayName();
    }
}
