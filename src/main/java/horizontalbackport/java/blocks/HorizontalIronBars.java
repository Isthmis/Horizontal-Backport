package horizontalbackport.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import horizontalbackport.HorizontalBackport;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.util.IIcon;

public class HorizontalIronBars extends HorizontalBase {
    public HorizontalIronBars() {
        super(Material.iron);
        this.setBlockName(HorizontalBackport.MODID + ":hgpIronBars");
        this.setBlockBounds(0, 0.4375f, 0, 1, 0.5625f, 1);
        this.setCreativeTab(CreativeTabs.tabDecorations);
        this.setResistance(6F);
        this.setHardness(5F);
        this.setStepSound(Block.soundTypeMetal);
        this.setLightOpacity(0);
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public int getRenderBlockPass() {
        return 1;
    }

    @Override
    public boolean canRenderInPass(int pass) {
        return pass == 1;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int metadata) {
        return Blocks.iron_bars.getIcon(side, metadata);
    }

    @Override
    public String getUnlocalizedName() {
        return Blocks.iron_bars.getUnlocalizedName();
    }
}
