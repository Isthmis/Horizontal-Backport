package horizontalbackport;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import horizontalbackport.blocks.HorizontalGlassPane;
import horizontalbackport.blocks.HorizontalIronBars;
import horizontalbackport.blocks.HorizontalStainedGlassPane;
import horizontalbackport.items.SubtypesModBlockItem;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

@Mod(
    modid = "hgp", // [IMPORTANT] Block ID compatibility for HGP
    name = "Horizontal Backport",
    version = "1.0.0"
)
public class HorizontalBackport {
    public static final String MODID = "hgp";

    @Instance(MODID)
    public static HorizontalBackport instance;

    public static Block horizontalGlassPane;
    public static Block horizontalStainedGlassPane;
    public static Block horizontalIronBars;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        horizontalGlassPane = new HorizontalGlassPane();
        horizontalStainedGlassPane = new HorizontalStainedGlassPane();
        horizontalIronBars = new HorizontalIronBars();

        GameRegistry.registerBlock(horizontalGlassPane, "hgpPane");
        GameRegistry.registerBlock(horizontalStainedGlassPane, SubtypesModBlockItem.class, "hgpColoredPane");
        GameRegistry.registerBlock(horizontalIronBars, "hgpIronBars");
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        // glass_pane -> hgpPane
        GameRegistry.addRecipe(new ItemStack(horizontalGlassPane, 3),
            "AAA",
            'A', new ItemStack(Blocks.glass_pane, 1)
        );
        // hgpPane -> glass_pane
        GameRegistry.addRecipe(new ItemStack(Blocks.glass_pane, 3),
            "A",
            "A",
            "A",
            'A', new ItemStack(horizontalGlassPane, 1)
        );
        // iron_bars -> hgpIronBars
        GameRegistry.addRecipe(new ItemStack(horizontalIronBars, 3),
            "AAA",
            'A', new ItemStack(Blocks.iron_bars, 1)
        );
        // hgpIronBars -> iron_bars
        GameRegistry.addRecipe(new ItemStack(Blocks.iron_bars, 3),
            "A",
            "A",
            "A",
            'A', new ItemStack(horizontalIronBars, 1)
        );

        // hgpColoredPane
        for (int i = 0; i <= 15; i++) {
            // stained_glass_pane -> hgpColoredPane
            GameRegistry.addRecipe(new ItemStack(horizontalStainedGlassPane, 3, i),
                "AAA",
                'A', new ItemStack(Blocks.stained_glass_pane, 1, i)
            );
            // hgpColoredPane -> stained_glass_pane
            GameRegistry.addRecipe(new ItemStack(Blocks.stained_glass_pane, 3, i),
                "A",
                "A",
                "A",
                'A', new ItemStack(horizontalStainedGlassPane, 1, i)
            );
        }
    }
}
